&emsp;&emsp;哈啰！各位萌新你们好啊&ensp;(•̀ᴗ•́)و ̑̑&ensp;！这里是校科协电子部，欢迎阅读我们推出的C语言新手教程。

&emsp;&emsp;要学习一门编程语言，代码是必须要写的，为了运行代码呢，就需要搭建相应的开发环境。对于C语言开发，比较简单的方法是安装Dev-cpp。

* [Dev-cpp安装教程](https://gitee.com/SAST-E/Toturial4Freshman/blob/master/C%E8%AF%AD%E8%A8%80/Dev-cpp%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B/%E3%80%90%E6%95%99%E7%A8%8B%E3%80%91Dev-cpp%E5%AE%89%E8%A3%85%E4%B8%8E%E7%AC%AC%E4%B8%80%E4%B8%AA%E7%A8%8B%E5%BA%8F.md)

&emsp;&emsp;不过，Dev-cpp的默认主题给人的视觉体验很难令人满意，所以我们可以对它进行一点美化。

* [Dev-cpp界面美化教程](https://gitee.com/SAST-E/Toturial4Freshman/blob/master/C%E8%AF%AD%E8%A8%80/Dev-cpp%E7%95%8C%E9%9D%A2%E7%BE%8E%E5%8C%96/%E3%80%90%E6%95%99%E7%A8%8B%E3%80%91Dev-cpp%E7%95%8C%E9%9D%A2%E7%BE%8E%E5%8C%96.md)

&emsp;&emsp;开发环境准备好了，就该一边看书一边敲代码了。书的选择是一个问题，在上一个十年中，谭浩强主编的书在高校中被广泛使用。但是，窃以为那只是因为作者的身份以及那些书在内容组织和习题设置上与“应试”极为契合罢了，不推荐。在马云家搜索，一本《C语言从入门到精通》的销量似乎不错，笔者也读过这本，内容只能说中规中矩，不推荐。

&emsp;&emsp;说来说去，在计算机领域，国内的基础性优质教材还是，嗯，很少。所以，笔者最终推荐的是这本——《C Primer Plus》，当然，有翻译版本。

&emsp;&emsp;然后，在书本之外，可以看一些教程、视频、文档作为辅助，这里列一个推荐清单：

### 视频教程类：

* [翁恺老师的C语言课](https://www.icourse163.org/course/ZJU-9001)

### 文档类：（当字典用）

* [菜鸟教程的文档](https://www.runoob.com/cprogramming/c-tutorial.html)

### 其他：

* [可以在线练习的C语言入门教程](https://www.imooc.com/learn/249)



&emsp;&emsp;当进行了一段时间的学习之后，你可能会觉得Dev-cpp的功能有些不够看，那个时候你可以选择一些更加现代的开发环境——比如Visual Studio Code、Atom等等。当然，对于C语言个人比较推荐使用CLion，暑假快结束的时候你可以去官网下一个，这时许可证只有30天。但是没关系，先用，开学以后用你的教育邮箱认证一下就有一年许可了，之后每年验证一次就行。另外，Visual Studio暂不推荐安装，因为学校C语言课通常会使用这个，不同版本VS共存可能会有问题（虽然现在这种情况出现的可能在逐渐变小）。



（补充：本文档目前由@路边的白桦 同志维护，如有问题请发邮件到dinyizhi@163.com）
&emsp;&emsp;哈啰！各位萌新你们好啊&ensp;(•̀ᴗ•́)و ̑̑&ensp;！这里是校科协电子部，欢迎阅读我们推出的新手教程。

&emsp;&emsp;本期我们来带大家安装Dev-cpp——一个经典、简洁的C语言开发软件。

&emsp;&emsp;首先，我们可以从[这里](https://sourceforge.net/projects/orwelldevcpp/files/latest/download)获取软件安装包，如果下载不正常，附件里也有提供（[Source目录](https://gitee.com/SAST-E/Toturial4Freshman/tree/master/C%E8%AF%AD%E8%A8%80/Dev-cpp%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B/Source)下）。

&emsp;&emsp;然后，开始之前碎碎念一波——建议大家安装软件之前先建几个文件夹给软件分一下类，比如建一个Software文件夹，然后下面建一些Video、Program之类的文件夹来安装软件。文件夹的划分如果不好确定的话可以画一个思维导图，推荐使用Xmind进行绘制。

&emsp;&emsp;（补充：本教程为没有任何基础的萌新设计，请放心阅读）

# 安装步骤

* 首先运行我们下载好的"······Setup.exe"这个安装包（双击或者右键打开什么的都可以啦）,会弹出下面的页面，等它加载（几秒钟而已）。

![](./Picture/DEV_cpp_1.png)

* 然后是语言选择界面，你会发现找不到中文，没关系，先选英文，中文一会就有了。

![](./Picture/DEV_cpp_2.png)

* 然后，软件安装时一般都会有的一个步骤，同意软件使用方面的一些条款，没什么特别的，选择“I Agree”即可。

![](./Picture/DEV_cpp_3.png)

* 下一步是选择安装目录，图里这里例子其实不是很好，更推荐“Software --> Program --> Dev-cpp”这样子的层级划分，另外补充一句，之后写的代码什么的请尽量和软件本体分开放，以免造成混乱。举个例子，你可以建一个文件夹叫“Code_Bank”，下面按软件或者按语言等等再划分一下，用来放代码。

* 选好了以后点“Install”就成。

  **警告：软件安装目录请不要有任何中文和空格，最好只使用英文和下划线，否则字符编码引发的问题可能导致软件无法正常使用**

![](./Picture/DEV_cpp_4.png)

* 然后等待一会，大概倒一杯水的时间。

![](./Picture/DEV_cpp_5.png)

* 然后会弹出一个界面进行语言的选择，现在有中文了。

![](./Picture/DEV_cpp_6.png)

* 下一步是选择主题，选默认的即可，虽然不太好看，但是下一篇教程里我们会做点美化。

![](./Picture/DEV_cpp_7.png)

* 然后所有设置就结束了，OK。

![](./Picture/DEV_cpp_8.png)

* 之后就会来到Dev-cpp的使用界面。

![](./Picture/DEV_cpp_9.png)

* 赶紧体验一下，先建个代码文件。

![](./Picture/DEV_cpp_10.png)

* 敲出经典的“Hello World！”程序，然后选个地方保存一下，名字嘛，习惯上第一个都叫做“Hello”，以后你会懂的（笑）。

  （补充：默认文件后缀为.cpp，其实这个是C++适用的文件，但是也完全兼容C语言，所以无需更改）

  代码可以参考这个：（不要复制哦）

  ```C
  #include <stdio.h>
  int main()
  {
  	printf("Hello World!");
  	return 0;
  }
  ```

![](./Picture/DEV_cpp_11.png)

* 点击图中的位置或者按快捷键F11编译并运行我们编写的程序。

  （补充：如果按F11是调节亮度之类的系统功能的话，请按Esc + Fn切换热键状态，之后原功能需要按Fn + F11使用）

![](./Picture/DEV_cpp_12.png)

* 运行之后会有一个这样的黑框框弹出来，里面显示了我们程序输出的内容“Hello World!”和一些提示信息。如果你也顺利地看到了这些，那么，恭喜，虽然你可能没有意识到，但是，新世界的大门已经打开，勇敢的少年/少女啊，准备好踏上探索星海的旅途了吗？

![](./Picture/DEV_cpp_13.png)

&emsp;&emsp;（推荐继续阅读美化Dev-cpp界面的相关内容，地址在[这里](https://gitee.com/SAST-E/Toturial4Freshman/blob/master/C%E8%AF%AD%E8%A8%80/Dev-cpp%E7%95%8C%E9%9D%A2%E7%BE%8E%E5%8C%96/%E3%80%90%E6%95%99%E7%A8%8B%E3%80%91Dev-cpp%E7%95%8C%E9%9D%A2%E7%BE%8E%E5%8C%96.md)）

